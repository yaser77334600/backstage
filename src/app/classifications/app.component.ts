import { Component, OnInit } from '@angular/core';
import { ClassificationsService } from 'src/app/providers/classifications.service';
import { NgForm } from '@angular/forms';
import { errorHandler } from '../utils';
declare var $: any;
declare function customInitFunctions();


@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {
  loading = true;
  classifications = [];
  info;
  selected;
  public linktheme = document.querySelector('#theme');

  constructor(private classificationsAPI: ClassificationsService) { }

  ngOnInit() {
    this.loadList();
    customInitFunctions();
    const url = localStorage.getItem('theme') || './assets/css/colors/default-dark.css';
    this.linktheme.setAttribute('href', url);
  }

  loadList() {
    this.classificationsAPI.getClassifications().subscribe((classifications: []) => {
      this.loading = false;
      this.classifications = classifications;
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  deleteClassification(id: string) {
    this.loading = true;
    this.classificationsAPI.deleteClassification(id).subscribe(() => {
      this.loadList();
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  openEdit(classification) {
    $('#editClassification').modal('show');
    this.selected = classification;
  }

  editClassification(form: NgForm) {
    this.classificationsAPI.updateClassification(this.selected._id, form.value)
    .subscribe((newClassification) => {
      $('#editClassification').modal('hide');
      this.loadList();
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  createClassification(form: NgForm) {
    this.loading = true;
    this.classificationsAPI.createClassification(form.value).subscribe((newClassification) => {
      form.reset();
      this.loadList();
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }
}
