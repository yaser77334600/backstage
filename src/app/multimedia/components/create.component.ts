import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { map } from 'rxjs/operators';
import { ClassificationsService } from 'src/app/providers/classifications.service';
import { GenresService } from 'src/app/providers/genres.service';
import { PeopleService } from 'src/app/providers/people.service';
import { Router } from '@angular/router';
import { LanguagesService } from 'src/app/providers/languages.service';
import { errorHandler } from 'src/app/utils';
declare var $: any;

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styles: []
})
export class CreateComponent implements OnInit {
  loading = false;
  mediaForm: FormGroup;
  classifications = [];
  classificationsSelected;
  languageSelected;
  languages = [];
  filterGenres = { name: '', limit: 10 };
  genres = [];
  genresSelected = [];
  people = { directors: [], actors: [] };
  peopleSelected = { directors: [], actors: [] };
  actorName = '';
  directorName = '';
  filterPeople = { name: '', limit: 10, category: 'director' };

  filter = { category: '', name: '', skip: 0, limit: 999 };

  info;
  dropdownSettings: any = {};
  ShowFilter = true;
  disabled = false;
  limitSelection = false;
  selectedItemsGenres: any[] = [];
  selectedItemsDirectors: any[] = [];
  selectedItemsActors: any[] = [];
  constructor(private fb: FormBuilder, private multimediaAPI: MultimediaService, private peopleAPI: PeopleService,
    private classificationAPI: ClassificationsService, private genresAPI: GenresService, private router: Router,
    private languageAPI: LanguagesService,) { }

  ngOnInit() {
    this.mediaForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(120)], this.mediaValidator.bind(this)),
      tagline: new FormControl('', [Validators.maxLength(80)]),
      description: new FormControl('', [Validators.maxLength(800)]),
      genresda: [this.selectedItemsGenres],
      genres:new FormArray([]),
      directorsadd: [this.selectedItemsDirectors],

      directors:new FormArray([]),

      actorsadd: [this.selectedItemsActors],

      actors:new FormArray([]),
      keywords: new FormArray([]),  
      language: new FormControl('', Validators.required),
      classification: new FormControl('', [Validators.required]),
    });
    this.loadClassifications();
    this.loadLanguages();
    this.loadGenres();
    this.loadDirectors()
    this.loadActors()
    //
    this.dropdownSettings = {
      singleSelection: false,
      idField: '_id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: this.ShowFilter,

    };
  }
  /**/
  onSelectGenres(item: any) {
    console.log(item._id)
    //this.selectedItemsGenres.push(item.idField)
  }

  loadGenres() {
    this.genresAPI.getGenres(this.filterGenres).subscribe((resp: []) => {
      this.genres = resp;
      console.log(this.genres)
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  loadDirectors() {
    this.filterPeople.category = 'director'
    this.peopleAPI.getPeople(this.filterPeople).subscribe((resp: any) => {

      this.people['directors'] = resp.people;
      
      }
    );
  }
  onSelectDirectors(item: any) {
    this.selectedItemsDirectors.push([item._id])
  }
  onSelectActors(item: any) {
    this.selectedItemsActors.push([item._id])
  }
  loadActors() {
    this.filterPeople.category = 'actor'
    console.log(this.filterPeople)
    this.peopleAPI.getPeople(this.filterPeople).subscribe((resp: any) => {

      this.people['actors'] = resp.people;
      console.log(this.people['actors'])
      }
    );
  }

  addPeople(personTmp, category) {
    if (!this.peopleSelected[category].some((person) => person._id === personTmp._id)) {
      this.peopleSelected[category].push(personTmp);
      (this.mediaForm.controls[category] as FormArray).push(new FormControl(personTmp._id));
    }
    this.people[category] = [];
    this.filterPeople.name = '';
    category === 'directors' ? this.directorName = '' : this.actorName = '';
  }

  removePerson(id: string, category) {
    let count = 0;
    (this.mediaForm.controls[category] as FormArray).value.forEach(val => {
      if (val === id) {
        (this.mediaForm.controls[category] as FormArray).removeAt(count);
        const pos = this.peopleSelected[category].map((person) => person._id).indexOf(id);
        this.peopleSelected[category].splice(pos, 1);
      }
      count++;
    });
  }

  searchPeople(category) {
    this.filterPeople.name = category === 'directors' ? this.directorName : this.actorName;
    category === 'directors' ? this.filterPeople.category = 'director' : this.filterPeople.category = 'actor';
    if (this.filterPeople.name.length > 0) {
      this.peopleAPI.getPeople(this.filterPeople).subscribe((resp: any) => { this.people[category] = resp.people; });
    } else {
      this.people[category] = [];
    }
  }

  submitMedia() {
    this.loading = true;
    console.log(this.mediaForm.value)
    const data=this.mediaForm.value
    console.log(data.genresda)
    
    for (let i = 0; i < data.genresda.length; i++) {
      const element = data.genresda[i]['_id']; 
      (this.mediaForm.controls['genres'] as FormArray).push(new FormControl(element));
    }
    for (let i = 0; i < data.directorsadd.length; i++) {
      const element = data.genresda[i]['_id']; 
      (this.mediaForm.controls['directors'] as FormArray).push(new FormControl(element));
    }
    for (let i = 0; i < data.actorsadd.length; i++) {
      const element = data.genresda[i]['_id']; 
      (this.mediaForm.controls['actors'] as FormArray).push(new FormControl(element));
    }
    console.log(this.mediaForm.value)

    this.multimediaAPI.createMedia(this.mediaForm.value).subscribe((resp: any) => {
      this.loading = false;
      this.router.navigate(['/multimedia/detail', resp._id]);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
   
  }

  validate(key) {
    return this.mediaForm.get(key).invalid && this.mediaForm.get(key).touched;
  }

  mediaValidator(control: AbstractControl) {
    return this.multimediaAPI.getMultimediaByName(control.value.trim()).pipe(map((media: any) => { 
      return media === null ? null : { mediaExist: true };
    }));
  }

  addKeyword(input) {
    if (input.value) {
      const keywords = this.mediaForm.controls.keywords as FormArray;
      keywords.push(new FormControl(input.value.trim()));
      input.value = '';
    }
  }

  removeKeyword(index) {
    (this.mediaForm.controls.keywords as FormArray).removeAt(index);
  }

  loadClassifications() {
    this.classificationAPI.getClassifications().subscribe((classifications: []) => {
      this.classifications = classifications;
    });
  }

  loadLanguages() {
    this.languageAPI.getLanguages().subscribe((languages: []) => {
      this.languages = languages;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  addClass(classTmp) {
    this.mediaForm.controls.classification.setValue(classTmp._id);
    this.classificationsSelected = classTmp.name;
    $('#addClassification').modal('hide');
  }

  selectLang(lang) {
    this.mediaForm.controls.language.setValue(lang._id);
    this.languageSelected = lang.code;
    $('#selectLanguage').modal('hide');
  }

  get getKeywords() { return (this.mediaForm.get('keywords') as FormArray).controls; }

}
