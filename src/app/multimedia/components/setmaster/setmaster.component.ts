import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-setmaster',
  templateUrl: './setmaster.component.html',
  styles: []
})
export class SetmasterComponent implements OnInit {
  masterForm: FormGroup;
  loadingTrailer = false;
  loadingMaster = false;
  sending = false;
  info;
  uploadRawTrailer = { status: '', message: 0 };
  uploadRawMaster = { status: '', message: 0 };
  multimedia;

  constructor(private fb: FormBuilder, private multimediaAPI: MultimediaService,
              private activeRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.multimedia = this.activeRoute.snapshot.params.id;
    this.masterForm = this.fb.group({ file: new FormControl(''), trailer: new FormControl('') });
  }

  submitMaster() {
    if (this.masterForm.value.file === '' && this.masterForm.value.trailer === '') {
      return true;
    }
    this.sending = true;
    const form = this.masterForm.value;
    if (form.trailer === '') {
      delete form.trailer;
    }
    this.multimediaAPI.setMasterList(this.multimedia, form)
    .subscribe((resp) => {
      this.router.navigate(['/multimedia/detail', this.multimedia]);
      this.sending = false;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.sending = false;
    });
  }

  uploadTrailer(event) {
    this.loadingTrailer = true;
    this.masterForm.get('trailer').setValue('');
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.multimediaAPI.uploadMedia(this.multimedia, file, { trailer: true }).subscribe((resp: any) => {
        this.uploadRawTrailer = resp;
        if (resp.key) {
          this.loadingTrailer = false;
          this.masterForm.get('trailer').setValue(resp.key);
        }
      }, (resp) => {
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
        this.loadingTrailer = false;
      });
    }
  }

  uploadMasterFile(event) {
    this.loadingMaster = true;
    this.masterForm.get('file').setValue('');
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.multimediaAPI.uploadMedia(this.multimedia, file).subscribe((resp: any) => {
        this.uploadRawMaster = resp;
        if (resp.key) {
          this.loadingMaster = false;
          this.masterForm.get('file').setValue(resp.key);
        }
      }, (resp) => {
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
        this.loadingMaster = false;
      });
    }
  }

  getInput(input): any {
    return this.masterForm.get(input) as FormControl;
  }

}
