import { Component, OnInit } from '@angular/core';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  multimedia = [];
  loading = true;
  info;
  total;
  filter = { skip: 0, limit: 20, name: '', status: '', onlyMe: true };
  constructor(private multimediaAPI: MultimediaService, private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadList();
  }

  loadList() {
    this.multimediaAPI.getMultimedia(this.filter).subscribe((resp: any) => {
      this.loading = false;
      this.multimedia = resp.multimedia;
      this.total =  Math.ceil(resp.total / this.filter.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  changePage(event) {
    this.filter.skip = event;
    const queryParams: Params = { skip: this.filter.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.loadList();
  }

}
