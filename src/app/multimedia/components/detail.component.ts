import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { ActivatedRoute, Params } from '@angular/router';
import { errorHandler } from 'src/app/utils';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
declare const videojs: any;
declare let $: any;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  multimedia;
  loading = true;
  sending = false;
  updating = false;
  info;
  infoFile;
  player;
  statusForm: FormGroup;
  user;
  type;

  constructor(private fb: FormBuilder, private activeRoute: ActivatedRoute, private multimediaAPI: MultimediaService,
              private auth: AuthService) {}

  ngOnInit() {
    this.user = this.auth.getUserInfo().user;
    this.activeRoute.params.subscribe((params: Params) => this.getMultimedia(params.id));
  }

  changeSource(type) {
    this.type = type;
  }

  getMultimedia(id) {
    this.loading = true;
    this.multimediaAPI.getSingleMultimedia(id).subscribe(media => {
      console.log(media)
      this.loading = false;
      this.multimedia = media;
      console.log(media);
      if (this.user.role === 'ADMIN') {
        this.statusForm = this.fb.group({
          status: new FormControl(this.multimedia.status, [Validators.required]),
          rejectedReason: new FormControl('', [Validators.required, Validators.max(3000)]),
        });
      }
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  changeMainImage(event, type) {
    this.sending = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.multimediaAPI.updateMainCover(this.multimedia._id, file, { type }).subscribe(multimedia => {
        this.multimedia = multimedia;
        this.sending = false;
      }, (resp) => {
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
        this.sending = false;
      });
    }
  }

  sendToReview() {
    this.loading = true;
    this.multimediaAPI.updateMedia({ status: 'pending' }, this.multimedia._id).subscribe(media => {
      this.loading = false;
      this.multimedia = media;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  isInvalidInput(input: string) {
    return this.statusForm.get(input).touched && this.statusForm.get(input).invalid ? true : false;
  }

  changeStatus() {
    this.updating = true;
    this.multimediaAPI.updateMedia(this.statusForm.value, this.multimedia._id).subscribe(media => {
      this.updating = false;
      this.multimedia = media;
      $('#updateModal').modal('hide');
    }, (resp) => {
      $('#updateModal').modal('hide');
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.updating = false;
    });
  }

  getInput(input) { return this.statusForm.get(input).value; }

}
