import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-detail-source',
  templateUrl: './detail-source.component.html',
  styles: []
})
export class DetailSourceComponent implements OnInit {
  info;
  loading = true;
  sending = false;
  loadingSub = false;
  loadingAudio = false;
  source: any = {};
  uploadSubtitleStatus = { status: '', message: 0 };
  uploadAudioStatus = { status: '', message: 0 };
  constructor(private activeRoute: ActivatedRoute, private multimediaAPI: MultimediaService, private router: Router) { }

  ngOnInit() {
    this.getSource(this.activeRoute.snapshot.params.source);
  }

  getSource(source) {
    this.loading = true;
    this.multimediaAPI.getSource(source)
    .subscribe((resp) => {
      console.log( resp)
      this.loading = false;
      this.source = resp;
      console.log( this.source)

    }, err => {
      this.loading = false;
    });
  }

  uploadAudio(event) {
    this.loadingAudio = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.multimediaAPI.uploadAudio(this.source._id, file).subscribe((resp: any) => {
        this.uploadAudioStatus = resp;
        if (resp._id) { this.router.navigate(['/multimedia/detail', resp._id]); }
      }, (resp) => {
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
        this.loadingAudio = false;
      });
    }
  }

  uploadSubtitle(event) {
    this.loadingSub = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.multimediaAPI.uploadSub(this.source._id, file).subscribe((resp: any) => {
        this.uploadSubtitleStatus = resp;
        if (resp._id) { this.router.navigate(['/multimedia/detail', resp._id]); }
      }, (resp) => {
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
        this.loadingSub = false;
      });
    }
  }

  changeMainImage(event, type) {
    this.sending = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      console.log(this.source._id)
      this.multimediaAPI.updateSourceCover(this.source._id, file, { type }).subscribe(source => {
        this.source = source;
        this.sending = false;
      }, (resp) => {
        this.sending = false;
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      });
    }
  }

}
