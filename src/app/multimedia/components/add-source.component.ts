import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { LanguagesService } from 'src/app/providers/languages.service';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-add-source',
  templateUrl: './add-source.component.html',
  styles: []
})
export class AddSourceComponent implements OnInit {
  sourceForm: FormGroup;
  sending = false;
  loading = false;
  loadingTrailer = false;
  uploadingVertical = false;
  uploadingHorizontal = false;
  languages = [];
  info;
  multimedia: string;
  uploadRawResponse = { status: '', message: 0 };
  uploadRawTrailer = { status: '', message: 0 };
  uploadCoverResponseX = { status: '', message: 0 };
  uploadCoverResponseY = { status: '', message: 0 };
  constructor(private fb: FormBuilder, private langAPI: LanguagesService, private multimediaAPI: MultimediaService,
              private activeRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.multimedia = this.activeRoute.snapshot.params.id;
    this.getLanguages();
    this.sourceForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(120)]),
      tagline: new FormControl('', [Validators.maxLength(80)]),
      description: new FormControl('', [Validators.maxLength(800)]),
      keywords: new FormArray([]),
      language: new FormControl('', [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
    });
  }

  getLanguages() {
    this.langAPI.getLanguages()
    .subscribe((langs: []) =>  this.languages = langs);
  }


  submitMedia() {
    this.sending = true;
    this.multimediaAPI.addSource(this.multimedia, this.sourceForm.value)
    .subscribe((res) => {
      this.sending = false;
      this.router.navigate(['multimedia/detail', this.multimedia]);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.sending = false;
    });
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  getInput(input): any {
    return this.sourceForm.get(input) as FormControl;
  }

  addKeyword(input) {
    if (input.value) {
      const keywords = this.sourceForm.controls.keywords as FormArray;
      keywords.push(new FormControl(input.value.trim()));
      input.value = '';
    }
  }

  removeKeyword(index) {
    (this.sourceForm.controls.keywords as FormArray).removeAt(index);
  }

}
