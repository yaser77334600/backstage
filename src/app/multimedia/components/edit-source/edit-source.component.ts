import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { LanguagesService } from 'src/app/providers/languages.service';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-edit-source',
  templateUrl: './edit-source.component.html',
  styles: ['']
})
export class EditSourceComponent implements OnInit {
  sourceForm: FormGroup;
  sending = false;
  loading = true;
  loadingTrailer = false;
  uploadingVertical = false;
  uploadingHorizontal = false;
  languages = [];
  info;
  source;
  uploadRawResponse = { status: '', message: 0 };
  uploadRawTrailer = { status: '', message: 0 };
  uploadCoverResponseX = { status: '', message: 0 };
  uploadCoverResponseY = { status: '', message: 0 };
  constructor(private fb: FormBuilder, private langAPI: LanguagesService, private multimediaAPI: MultimediaService,
              private activeRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getSource(this.activeRoute.snapshot.params.source);
  }

  getSource(source) {
    this.loading = true;
    this.multimediaAPI.getSource(source)
    .subscribe((resp) => {
      this.source = resp;
      this.sourceForm = this.fb.group({
        name: new FormControl(this.source.name, [Validators.required, Validators.maxLength(120)]),
        tagline: new FormControl(this.source.tagline, [Validators.maxLength(80)]),
        description: new FormControl(this.source.description, [Validators.maxLength(800)]),
        keywords: new FormArray([]),
      });
      this.source.keywords.forEach(key => this.addKeyword({ value: key }));
      this.loading = false;
    }, resp => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }


  submitMedia() {
    this.sending = true;
    this.multimediaAPI.updateSource(this.source._id, this.sourceForm.value)
    .subscribe((res) => {
      this.sending = false;
      this.router.navigate(['multimedia/detailsource', this.source._id]);
      console.log(res);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.sending = false;
    });
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  getInput(input): any {
    return this.sourceForm.get(input) as FormControl;
  }

  addKeyword(input) {
    if (input.value) {
      const keywords = this.sourceForm.controls.keywords as FormArray;
      keywords.push(new FormControl(input.value.trim()));
      input.value = '';
    }
  }

  removeKeyword(index) {
    (this.sourceForm.controls.keywords as FormArray).removeAt(index);
  }
}
