import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { ClassificationsService } from 'src/app/providers/classifications.service';
import { GenresService } from 'src/app/providers/genres.service';
import { PeopleService } from 'src/app/providers/people.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LanguagesService } from 'src/app/providers/languages.service';
import { errorHandler } from 'src/app/utils';
declare var $: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})
export class EditComponent implements OnInit {
  multimedia;
  loading = true;
  sending = false;
  mediaForm: FormGroup;
  classifications = [];
  classificationsSelected;
  languageSelected;
  languages = [];
  filterGenres = { name: '', limit: 10 };
  genres = [];
  genresSelected = [];
  people = { directors: [], actors: []  };
  peopleSelected = { directors: [], actors: []  };
  actorName = '';
  directorName = '';
  filterPeople = { name: '', limit: 10, category: 'director'  };
  info;
  constructor(private fb: FormBuilder, private multimediaAPI: MultimediaService, private peopleAPI: PeopleService,
              private classificationAPI: ClassificationsService, private genresAPI: GenresService, private router: Router,
              private languageAPI: LanguagesService, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getMultimedia(this.activeRoute.snapshot.params.id);
  }

  getMultimedia(id) {
    this.multimediaAPI.getSingleMultimedia(id).subscribe((media: any) => {
      this.multimedia = media;
      this.mediaForm = this.fb.group({
      name: new FormControl(media.name, [Validators.required, Validators.maxLength(120)]),
      tagline: new FormControl(media.tagline, [Validators.maxLength(80)]),
      description: new FormControl(media.description, [Validators.maxLength(800)]),
      genres: new FormArray([]),
      directors: new FormArray([]),
      actors: new FormArray([]),
      keywords: new FormArray([]),
      language: new FormControl('', Validators.required),
      classification: new FormControl(media.classification, [Validators.required]),
    });
      this.multimedia.genres.forEach(genre => this.addGenres(genre));
      this.multimedia.directors.forEach(director => this.addPeople(director, 'directors'));
      this.multimedia.actors.forEach(actors => this.addPeople(actors, 'actors'));
      this.multimedia.keywords.forEach(key => this.addKeyword({ value: key }));
      this.selectLang(this.multimedia.language);
      this.loadClassifications();
      this.addClass(this.multimedia.classification);
      this.loadLanguages();
      this.loading = false;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  addGenres(genresTmp) {
    if (!this.genresSelected.some((genres) => genres._id === genresTmp._id)) {
      this.genresSelected.push(genresTmp);
      (this.mediaForm.controls.genres as FormArray).push(new FormControl(genresTmp._id));
    }
    this.genres = [];
    this.filterGenres.name = '';
  }

  removeGenres(id: string) {
    let count = 0;
    (this.mediaForm.controls.genres as FormArray).value.forEach(val => {
      if (val === id) {
        (this.mediaForm.controls.genres as FormArray).removeAt(count);
        const pos = this.genresSelected.map((genre) =>  genre._id).indexOf(id);
        this.genresSelected.splice(pos, 1);
      }
      count++;
    });
  }

  searchGenres() {
    if (this.filterGenres.name.length > 0) {
    this.genresAPI.getGenres(this.filterGenres).subscribe((resp: []) => { this.genres = resp; });
    } else {
      this.genres = [];
    }
  }

  addPeople(personTmp, category) {
    if (!this.peopleSelected[category].some((person) => person._id === personTmp._id)) {
      this.peopleSelected[category].push(personTmp);
      (this.mediaForm.controls[category] as FormArray).push(new FormControl(personTmp._id));
    }
    this.people[category] = [];
    this.filterPeople.name = '';
    category === 'directors' ? this.directorName = '' : this.actorName = '';
  }

  removePerson(id: string, category) {
    let count = 0;
    (this.mediaForm.controls[category] as FormArray).value.forEach(val => {
      if (val === id) {
        (this.mediaForm.controls[category] as FormArray).removeAt(count);
        const pos = this.peopleSelected[category].map((person) =>  person._id).indexOf(id);
        this.peopleSelected[category].splice(pos, 1);
      }
      count++;
    });
  }

  searchPeople(category) {
    this.filterPeople.name = category === 'directors' ? this.directorName : this.actorName;
    category === 'directors' ? this.filterPeople.category = 'director' : this.filterPeople.category = 'actor';
    if (this.filterPeople.name.length > 0) {
      this.peopleAPI.getPeople(this.filterPeople).subscribe((resp: any) => {
        this.people[category] = resp.people;
      }, (resp) => {
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      });
    } else {
      this.people[category] = [];
    }
  }

  submitMedia() {
    this.sending = true;
    this.multimediaAPI.updateMedia(this.mediaForm.value, this.multimedia._id).subscribe((resp: any) => {
      this.sending = false;
      this.router.navigate(['/multimedia/detail', resp._id]);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.sending = false;
    });
  }

  validate(key) {
    return this.mediaForm.get(key).invalid && this.mediaForm.get(key).touched;
  }

  addKeyword(input) {
    if (input.value) {
      const keywords = this.mediaForm.controls.keywords as FormArray;
      keywords.push(new FormControl(input.value.trim()));
      input.value = '';
    }
  }

  removeKeyword(index) {
      (this.mediaForm.controls.keywords as FormArray).removeAt(index);
  }

  loadClassifications() {
    this.classificationAPI.getClassifications().subscribe((classifications: []) => {
      this.classifications =  classifications;
    });
  }

  loadLanguages() {
    this.languageAPI.getLanguages().subscribe((languages: []) =>  this.languages = languages);
  }

  addClass(classTmp) {
    this.mediaForm.controls.classification.setValue(classTmp._id);
    this.classificationsSelected = classTmp.name;
    $('#addClassification').modal('hide');
  }

  selectLang(lang) {
    this.mediaForm.controls.language.setValue(lang._id);
    this.languageSelected = lang.code;
    $('#selectLanguage').modal('hide');
  }

  get getKeywords() { return (this.mediaForm.get('keywords') as FormArray).controls; }
}
