import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all.component';
import { CreateComponent } from './components/create.component';
import { EditComponent } from './components/edit.component';
import { DetailComponent } from './components/detail.component';
import { AuthGuard } from '../auth/auth.guard';
import { AddSourceComponent } from './components/add-source.component';
import { DetailSourceComponent } from './components/detail-source/detail-source.component';
import { SetmasterComponent } from './components/setmaster/setmaster.component';
import { EditSourceComponent } from './components/edit-source/edit-source.component';
import { AccountSettingsComponent } from '../shared/account-settings/account-settings.component';

export const multimediaRoutes: Routes = [
    { path: 'multimedia', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'add', component: CreateComponent },
        { path: 'edit/:id', component: EditComponent  },
        { path: 'detail/:id', component: DetailComponent },
        { path: 'detailsource/:source', component: DetailSourceComponent },
        { path: 'addsource/:id', component: AddSourceComponent },
        { path: 'editsource/:source', component: EditSourceComponent },
        { path: 'setmaster/:id', component: SetmasterComponent },
        { path: 'account-Settings', component: AccountSettingsComponent},
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
