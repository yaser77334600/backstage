import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { AllComponent } from './components/all.component';
import { CreateComponent } from './components/create.component';
import { DetailComponent } from './components/detail.component';
import { EditComponent } from './components/edit.component';
import { AddSourceComponent } from './components/add-source.component';
import { DetailSourceComponent } from './components/detail-source/detail-source.component';
import { SetmasterComponent } from './components/setmaster/setmaster.component';
import { EditSourceComponent } from './components/edit-source/edit-source.component';


import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { multimediaRoutes } from './app.routing'; 
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  declarations: [
    AppComponent,
    AllComponent,
    CreateComponent,
    DetailComponent,
    EditComponent,
    AddSourceComponent,
    DetailSourceComponent,
    SetmasterComponent,
    EditSourceComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forRoot(multimediaRoutes),
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class MultimediaModule { }
