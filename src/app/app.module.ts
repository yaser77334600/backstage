import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// Modulos
import { ROUTES } from './app-routing.module';
import { PagesModule } from './pages/pages.module';
// import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { AuthComponent } from './auth/auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { MultimediaModule } from './multimedia/multimedia.module';
import { SharedModule } from './shared/shared.module';
// import { NavbarComponent } from './shared/navbar/navbar.component';
// import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { RouterModule, Routes } from '@angular/router';
import { ClassificationsModule } from './classifications/classifications.module';
import { GenresModule } from './genres/genres.module';
import { LanguagesModule } from './languages/languages.module';
import { PeopleModule } from './people/people.module';
import { UsersModule } from './users/users.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// import { StatisticsModule } from './statistics/statistics.module';
import { StatisticsModule } from './statistics/statistics.module';


@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    NopagefoundComponent,
    // NavbarComponent,
    // SidebarComponent,
  ],
  imports: [
    RouterModule.forRoot( ROUTES ),
    BrowserModule,
    ClassificationsModule,
    GenresModule,
    LanguagesModule,
    PeopleModule,
    UsersModule,
    PagesModule,
    FormsModule,
    HttpClientModule,
    MultimediaModule,
    SharedModule,
    StatisticsModule,
    NgMultiSelectDropDownModule.forRoot()
    // AuthModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
