import { Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';

export const ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'multimedia'},
];
