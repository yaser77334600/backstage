import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all.component';
import { CreateComponent } from './components/create.component';
import { EditComponent } from './components/edit.component';
import { DetailComponent } from './components/detail.component';
import { AuthGuard } from '../auth/auth.guard';

export const peopleRoutes: Routes = [
    { path: 'people', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'add', component: CreateComponent },
        { path: 'edit/:id', component: EditComponent  },
        { path: 'detail/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
