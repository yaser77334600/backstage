import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/app/providers/people.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  total;
  loading = true;
  people = [];
  info;
  filter = { category: '', name: '', skip: 0, limit: 10 };
  validcategories = ['director', 'actor'];
  constructor(private peopleAPI: PeopleService, private activeRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((query) => {
      this.filter.category = query.category || 'actor';
      this.filter.skip = Number(query.skip) || 0;
      this.loadList();
    });
  }

  loadList() {
    this.peopleAPI.getPeople(this.filter).subscribe((resp: { people: [], total: number }) => {
      this.loading = false;
      this.people = resp.people;
      this.total =  Math.ceil(resp.total / this.filter.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  changePage(event) {
    this.filter.skip = event;
    const queryParams: Params = { skip: this.filter.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.loadList();
  }

  changeFilter(category) {
    if (this.validcategories.indexOf(category) !== -1) {
      this.router.navigate([], { relativeTo: this.activeRoute, queryParams: { category } });
      this.filter.skip = 0;
      this.filter.category = category;
      this.loadList();
    }
  }

}
