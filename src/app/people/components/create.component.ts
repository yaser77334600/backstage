import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { PeopleService } from 'src/app/providers/people.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styles: []
})
export class CreateComponent implements OnInit {
  personForm: FormGroup;
  loading = false;
  person = true;
  info;
  constructor(private fb: FormBuilder, private peopleAPI: PeopleService, private router: Router) { }

  ngOnInit() {
    this.personForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(60)], this.personValidator.bind(this)),
      gender: new FormControl('', [Validators.required]),
      birthdate: new FormControl('', [Validators.required]),
      category: new FormControl('actor', [Validators.required]),
      biography: new FormControl('', [Validators.required, Validators.maxLength(2000)]),
    });
  }

  submitPerson() {
    const newPerson = this.personForm.value;
    newPerson.birthdate = new Date(newPerson.birthdate).toISOString();
    this.loading = true;
    this.peopleAPI.createPerson(this.personForm.value).subscribe((person: any) => {
      this.loading = false;
      this.router.navigate(['/people/detail', person._id]);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  validate(key) {
    return this.personForm.get(key).invalid && this.personForm.get(key).touched;
  }

  get biography() {
    return this.personForm.get('biography').value;
  }

  personValidator(control: AbstractControl) {
    return this.peopleAPI.getPersonByName(control.value.trim()).pipe(map((people: any) => {
      return people === null ? null : { peopleExist: true };
    }));
  }
}
