import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/app/providers/people.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-edit',
  templateUrl: './create.component.html',
  styles: []
})
export class EditComponent implements OnInit {
  personForm: FormGroup;
  person;
  info;
  loading = false;
  constructor(private activeRoute: ActivatedRoute, private peopleAPI: PeopleService,
              private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.loadList(params.id);
    });
  }

  loadList(id) {
    this.peopleAPI.getPerson(id).subscribe((person: any) => {
      this.person = person;
      this.personForm = this.fb.group({
        name: new FormControl(person.name, [Validators.required, Validators.maxLength(60)]),
        gender: new FormControl(person.gender, [Validators.required]),
        birthdate: new FormControl(person.birthdate.substr(0, 10), [Validators.required]),
        category: new FormControl(person.category, [Validators.required]),
        biography: new FormControl(person.biography, [Validators.required, Validators.maxLength(2000)]),
      });
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  submitPerson() {
    const newPerson = this.personForm.value;
    newPerson.birthdate = new Date(newPerson.birthdate).toISOString();
    this.loading = true;
    this.peopleAPI.updatePerson(this.person._id, this.personForm.value).subscribe((person: any) => {
      this.loading = false;
      this.router.navigate(['/people/detail', person._id]);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  validate(key) {
    return this.personForm.get(key).invalid && this.personForm.get(key).touched;
  }

  get biography() {
    return this.personForm.get('biography').value;
  }

}
