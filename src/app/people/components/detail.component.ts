import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PeopleService } from 'src/app/providers/people.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  person;
  info;
  infoFile;
  loading = true;
  sending = false;
  constructor(private activeRoute: ActivatedRoute, private peopleAPI: PeopleService) {
  }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.loadList(params.id);
    });
  }


  loadList(id) {
    this.loading = true;
    this.peopleAPI.getPerson(id).subscribe(person => {
      this.loading = false;
      this.person = person;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  changeProfileImg(event) {
    this.sending = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.peopleAPI.updatePhoto(this.person._id, file).subscribe(person => {
        this.person = person;
        this.infoFile = { message: 'Image updated successfully', class: 'alert alert-success', persist: false, show: true };
        this.sending = false;
      }, (resp) => {
        const content = errorHandler(resp);
        this.infoFile = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
        this.sending = false;
      });
    }
  }

}
