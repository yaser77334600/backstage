import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../providers/settings.service';

declare function customInitFunctions();

@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {

  constructor(private settingsService:SettingsService) { }

  ngOnInit() {
    customInitFunctions();
  }

}
