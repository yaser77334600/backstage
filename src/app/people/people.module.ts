import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all.component';
import { CreateComponent } from './components/create.component';
import { EditComponent } from './components/edit.component';
import { DetailComponent } from './components/detail.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { peopleRoutes } from './app.routing';

@NgModule({
  declarations: [AppComponent, AllComponent, CreateComponent, EditComponent, DetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forRoot(peopleRoutes),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PeopleModule { }
