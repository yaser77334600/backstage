import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';
import { AllComponent } from './components/all/all.component';

export const statisticsRoutes: Routes = [
    {path: 'statistics', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]}
]