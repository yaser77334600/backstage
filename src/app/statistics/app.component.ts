import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../providers/settings.service';
// import { ExcelInformeService } from '../providers/excel-informe.service';
// import { data } from '../shared/models/excelInformeUno';
declare function customInitFunctions();
@Component({
    selector: 'app-app',
    templateUrl: './app.component.html',
    styles: []
  })
  export class AppComponent implements OnInit {

    constructor( private settingsService:SettingsService ) { }
  
    ngOnInit() {
      customInitFunctions()
      console.log( 'Init de statistics' )
      // this.excelInformeService.generateExcel(data);
    }

    exportar(){
      // this.excelInformeService.generateExcel(data);
      console.log('Funcion exportar');
    }
  
  }