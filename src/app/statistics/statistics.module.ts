import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { statisticsRoutes } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AllComponent } from './components/all/all.component';

@NgModule({
  declarations: [
    AppComponent,
    AllComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forRoot(statisticsRoutes),
  ]
})
export class StatisticsModule { }
