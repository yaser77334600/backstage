import { Component, OnInit } from '@angular/core';
 
import * as Excel from "exceljs/dist/exceljs.min.js"
import * as ExcelProper from "exceljs";

import { ExcelInformeService } from 'src/app/providers/excel-informe.service';
import { data } from '../../../shared/models/excelInformeUno';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html', 
})
export class AllComponent implements OnInit {

  json_data=[{
		"name": "Raja",
		"age": 20
	},
	{
		"name": "Mano",
		"age": 40
	},
	{
		"name": "Tom",
		"age": 40
	},
	{
		"name": "Devi",
		"age": 40
	},
	{
		"name": "Mango",
		"age": 40
	}
]

  constructor() {}

  ngOnInit() {
  }
  exportar() {
    //this.exportExcel.exportAsExcelFile(data, 'data')
    // this.exportExcel.generateExcel(data)

    console.log("Estas en Statistics");
  }

  downloadExcel(){

    // Creando un nuevo libro de excel
    let workbook = new Workbook(); 
    // Añadiendo hoja de trabajo
    let worksheet = workbook.addWorksheet( "Datos de uvu" );
    let header=["Name","Age"]
    let headerRow = worksheet.addRow(header);
    for (let x1 of this.json_data)
    {
        let x2=Object.keys(x1);
        let temp=[]
        for(let y of x2)
        {
          temp.push(x1[y])
        }
        worksheet.addRow(temp)
    }

  //set downloadable file name
  let fname="Emp Data Sep 2020"

  //add data and file name and download
  workbook.xlsx.writeBuffer().then((data) => {
    let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    fs.saveAs(blob, fname+'-'+new Date().valueOf()+'.xlsx');
  });

  }

}
