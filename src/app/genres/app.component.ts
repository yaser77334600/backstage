import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GenresService } from '../providers/genres.service';
import { errorHandler } from '../utils';
import { SettingsService } from '../providers/settings.service';
declare var $: any;
declare function customInitFunctions();
@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {
  loading = true;
  genres = [];
  info;
  selected;
  constructor(private genreAPI: GenresService, 
              private settingsService:SettingsService
) { }

  ngOnInit() {
    this.loadList();
    customInitFunctions();
  }

  loadList() {
    this.genreAPI.getGenres().subscribe((genres: []) => {
      this.loading = false;
      this.genres = genres;
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  deleteGenre(id: string) {
    this.loading = true;
    this.genreAPI.deleteGenre(id).subscribe(() => {
      this.loadList();
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  openEdit(genre) {
    $('#editGenre').modal('show');
    this.selected = genre;
  }

  editGenre(form: NgForm) {
    this.genreAPI.updateGenre(this.selected._id, form.value)
    .subscribe((newGenre) => {
      $('#editGenre').modal('hide');
      this.loadList();
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  createGenre(form: NgForm) {
    this.loading = true;
    this.genreAPI.createGenre(form.value).subscribe(() => {
      form.reset();
      this.loadList();
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }
}
