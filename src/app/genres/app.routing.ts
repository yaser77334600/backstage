import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';

export const genresRoutes: Routes = [
    { path: 'genres', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'detail/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
