import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { errorHandler } from 'src/app/utils';
import { GenresService } from 'src/app/providers/genres.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent {
  loading = true;
  genres = [];
  info;
  selected;
  constructor(private genreAPI: GenresService) {
    this.loadList();
  }

  loadList() {
    this.genreAPI.getGenres().subscribe((genres: []) => {
      this.loading = false;
      this.genres = genres;
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  createGenre(form: NgForm) {
    this.loading = true;
    this.genreAPI.createGenre(form.value).subscribe(() => {
      form.reset();
      this.loadList();
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }
}
