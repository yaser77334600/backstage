import { Component, OnInit } from '@angular/core';
import { GenresService } from 'src/app/providers/genres.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';
import { MultimediaService } from 'src/app/providers/multimedia.service';
import { NgModel } from '@angular/forms';
declare let $: any;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent {
  loading = true;
  searching = true;
  multimedia = [];
  genre;
  info;
  update;
  filter = { skip: 0, limit: 20, name: '', /*status: 'published'*/ };
  constructor(private genreAPI: GenresService, private activeRoute: ActivatedRoute,
              private multimediaAPI: MultimediaService, private router: Router) {
    this.getGenre(this.activeRoute.snapshot.params.id);
    this.getMedia();
  }

  getMedia() {
    this.searching = true;
    this.multimediaAPI.getMultimedia(this.filter).subscribe((resp: any) => {
      console.log(resp)
      this.searching = false;
      this.multimedia = resp.multimedia;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.searching = false;
    });
  }

  getGenre(id) {
    this.genreAPI.getGenre(id).subscribe((genre) => {
      this.loading = false;
      this.genre = genre;
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  addFeatured(id) {
    this.updateGenre({ featuredMedia: [id] });
  }

  deleteFeatured(id) {
    this.updateGenre({ removeFromFeatured: [id] });
  }

  updateName(name: NgModel) {
    $('#updateModal').modal('hide');
    this.updateGenre({ name: name.value.trim() });
    name.reset();
  }

  updateGenre(update) {
    this.loading = true;
    this.genreAPI.updateGenre(this.genre._id, update)
    .subscribe((genre) => {
      this.genre = genre;
      this.getGenre(this.genre._id);
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  deleteGenre() {
    this.loading = true;
    this.genreAPI.deleteGenre(this.genre._id)
    .subscribe(() => {
      $('#deleteModal').modal('hide');
      this.loading = false;
      this.router.navigate(['/genres/all']);
    }, (resp: any) => {
      $('#deleteModal').modal('hide');
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

}
