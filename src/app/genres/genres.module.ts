import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { genresRoutes } from './app.routing';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';

@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forRoot(genresRoutes),
    FormsModule,
  ]
})
export class GenresModule { }
