import LaguagesControlKeys from "./laguagesControlKeys";

interface LanguageModel {
  [LaguagesControlKeys.LANGUAGE]: string;
}

export default LanguageModel;
