import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';

export const languagesRoutes: Routes = [
    { path: 'languages', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: '', component: AppComponent },
        { path: '**', component: AppComponent },
    ]
    }
];
