import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import { LanguagesService } from '../providers/languages.service';
import { errorHandler } from '../utils';
import LaguagesControlKeys from "./models/laguagesControlKeys";
declare function customInitFunctions(); 
import LanguageModel from "./models/languageModel";
declare var $: any;

@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent implements OnInit {
  loading = true;
  languages = [];
  info;
  selected;
  public controlKeys = LaguagesControlKeys;
  public readonly formGroup: FormGroup = new FormGroup({
    [this.controlKeys.LANGUAGE]: new FormControl(null, [Validators.required])
  });

  constructor(private languageAPI: LanguagesService) { }

  ngOnInit() {
    this.loadList();
    customInitFunctions();
  }

  loadList() {
    this.languageAPI.getLanguages().subscribe((languages: []) => {
      this.loading = false;
      this.languages = languages;
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  deleteLanguage(id: string) {
    this.loading = true;
    this.languageAPI.deleteLanguages(id).subscribe(() => {
      this.loadList();
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  openEdit(language) {
    $('#editLanguage').modal('show');
    this.selected = language;
  }

  editLanguage(form: NgForm) {
    this.languageAPI.updateLanguages(this.selected._id, form.value)
    .subscribe((newLanguage) => {
      $('#editLanguage').modal('hide');
      this.loadList();
    }, (resp: any) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  createLanguage(): void {
    this.loading = true;
    this.languageAPI.createLanguages(this.formGroup.get([this.controlKeys.LANGUAGE]).value).subscribe(() => {
      this.formGroup.reset();
      this.loadList();
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }
}
