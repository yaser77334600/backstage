import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { languagesRoutes } from './app.routing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forRoot(languagesRoutes),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LanguagesModule { }
