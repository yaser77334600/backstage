import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from 'src/app/providers/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})
export class EditComponent {
  loading = true;
  updating = false;
  user;
  info;
  accountForm: FormGroup;
  countries;
  constructor(private fb: FormBuilder, private userAPI: UsersService, private activeRoute: ActivatedRoute,
              private http: HttpClient, private router: Router) {
    this.getCountriesXML();
    this.getUser(this.activeRoute.snapshot.params.id);
  }

  getUser(id) {
    this.loading = true;
    this.userAPI.getUser(id).subscribe((user: any) => {
      this.loading = false;
      this.user = user;
      this.accountForm = this.fb.group({
        name: new FormControl(user.name ? user.name : '', [Validators.required, Validators.maxLength(30)]),
        // tslint:disable-next-line:max-line-length
        phone: new FormControl(user.phone ? user.phone : '', [Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern(/^[0-9]+$/)]),
        country: new FormControl(user.country ? user.country : '', [Validators.required, Validators.minLength(2)]),
        // tslint:disable-next-line:max-line-length
        address: new FormControl(user.address ? user.address : '', [Validators.required, Validators.minLength(5), Validators.maxLength(240)]),
        role: new FormControl(user.role ? user.role : '', [Validators.required]),
      });
      console.log(user);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  getCountriesXML() {
    this.http.get('assets/countries.json').toPromise()
    .then((data: []) =>  this.countries = data).catch((e) => console.log(e));
  }

  getInput(input) {
    return this.accountForm.get(input) as FormControl;
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  updateAccount() {
    this.updating = true;
    this.userAPI.updateUser(this.user._id, this.accountForm.value)
    .subscribe((user: any) => {
      this.updating = false;
      this.router.navigate(['/users/detail', this.user._id]);
    }, (resp) => {
      this.updating = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger' };
    });
  }

}
