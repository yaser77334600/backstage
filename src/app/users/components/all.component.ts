import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/providers/users.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  users = [];
  filter = { name: '', email: '', skip: 0, limit: 10 };
  loading = true;
  total;
  info;
  constructor(private userAPI: UsersService, private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadList();
  }

  loadList() {
    this.loading = true;
    this.userAPI.getUsers(this.filter).subscribe((resp: any) => {
      this.loading = false;
      this.users = resp.users;
      this.total =  Math.ceil(resp.total / this.filter.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }

  changePage(event) {
    this.filter.skip = event;
    const queryParams: Params = { skip: this.filter.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.loadList();
  }

}
