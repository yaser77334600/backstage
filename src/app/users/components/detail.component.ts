import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/providers/users.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SubscriptionsService } from 'src/app/providers/subscriptions.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  user;
  info;
  loading = true;
  loadingSub = true;
  infoSub;
  subscription;
  constructor(private userAPI: UsersService, private activeRoute: ActivatedRoute, private router: Router,
              private subscriptionAPI: SubscriptionsService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.loadList(params.id);
    });
  }

  loadList(id) {
    this.userAPI.getUser(id).subscribe(user => {
      this.loading = false;
      this.user = user;
      console.log(user);
      this.loadSubscription();
    }, (resp) => {
      let message = 'An error has occurred';
      this.loading = false;
      if (resp.status === 404) { message = 'User not found'; }
      this.info = { message, class: 'alert alert-danger', persist: true, show: true };
    });
  }

  loadSubscription() {
    if ('customer' in this.user) {
      this.subscriptionAPI.getSubscription(this.user.customer).subscribe((data) => {
        this.subscription = data;
        console.log(data);
        this.loadingSub = false;
      }, (error) => {
        this.loadingSub = false;
        console.log(error);
      });
    } else {
      this.loadingSub = false;
      this.infoSub = { message: 'This user has no active subscriptions', class: 'alert alert-info', persist: true, show: true };
    }
  }

}
