import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailComponent } from './components/detail.component';
import { AllComponent } from './components/all.component';
import { EditComponent } from './components/edit.component';
import { AppComponent } from './app.component';
import { CreateComponent } from './components/create.component';
import { RouterModule } from '@angular/router';
import { usersRoutes } from './app.routing';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DetailComponent, AllComponent, EditComponent, AppComponent, CreateComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(usersRoutes)
  ]
})
export class UsersModule { }
