import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loading = false;
  info;
  constructor(private auth: AuthService, private router: Router) { }

  login(user) {
    this.loading = true;
    this.auth.login(user.value).subscribe(data => {
      this.router.navigateByUrl('/multimedia');
      this.loading = false;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.loading = false;
    });
  }


}
