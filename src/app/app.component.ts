import { Component, OnInit } from '@angular/core';
import { SettingsService } from './providers/settings.service';

declare function customInitFunctions();


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  title = 'analytics';

  constructor( private settingsService:SettingsService ){}

  ngOnInit(): void{
    customInitFunctions();
  }

}
