import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../providers/settings.service';

@Component({
  selector: 'app-breadcrumps',
  templateUrl: './breadcrumps.component.html',
  styles: [
  ]
})
export class BreadcrumpsComponent implements OnInit {

    // Propiedades de Clase
    public  linkTheme = document.querySelector('#theme');
    public linksS: NodeListOf<Element>;
  
  constructor( public settingsService: SettingsService ) { }

  ngOnInit(): void {
    this.settingsService.checkCurrentTime();
  }

  changeTheme(theme: string){
    this.settingsService.changeTheme(theme);
  }

}
