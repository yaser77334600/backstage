import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styles: []
})
export class PaginationComponent  {
  @Input() total;
  @Input() limit;
  @Output() changePage: EventEmitter <number>;
  @Input() skip;
  selectedIndex = 0;
  constructor() {
    this.changePage = new EventEmitter();
  }

  toSkip(skip, index) {
    this.changePage.emit(skip);
    this.selectedIndex = index;
    window.scroll(0, 0);
  }

}
