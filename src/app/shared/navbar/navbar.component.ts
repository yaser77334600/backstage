import { Component } from '@angular/core';
// import { AuthService } from 'src/app/auth/auth.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: ['']
})
export class NavbarComponent {
  constructor(public auth: AuthService) { }

  logout() {
    this.auth.logOut();
  }

}
