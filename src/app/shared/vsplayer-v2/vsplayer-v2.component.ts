import { Component, OnInit, Input } from '@angular/core';
import { createFormData, errorHandler } from 'src/app/utils';
import { AuthService } from 'src/app/auth/auth.service';
import { MultimediaService } from 'src/app/providers/multimedia.service';
// import { VgAPI } from 'videogular2/compiled/core';
// import { VgAPI } from '@videogular/ngx-videogular/core';


@Component({
  selector: 'app-vsplayer-v2',
  templateUrl: './vsplayer-v2.component.html',
  styles: ['vg-player { height: auto!important; }']
})
export class VsplayerV2Component implements OnInit {
  @Input() options: {
    autoplay: boolean,
    sources: { src: string, type: string, }[],
    preload: boolean,
    id,
    poster,
  };
  token;
  show = true;
  info;
  constructor(private auth: AuthService, private mediaAPI: MultimediaService) { }

  ngOnInit() {
    this.token = createFormData({ tk: this.auth.returnToken() });
    if (this.options.sources.length > 0) { this.options.sources.forEach(source => source.src += '?' + this.token); }
  }

  onVideoError(event) {
    console.log(event);
    this.show = false;
    if (this.options.id && this.options.id !== '') {
      this.mediaAPI.playMedia(this.options.id).subscribe((data: any) => {
        this.info = { show: true,  message: 'Something went wrong...', class: 'alert alert-danger', persist: true };
      }, async (resp) => {
        const content = errorHandler(resp);
        if (['S001', 'S002', 'S003', 'S004'].includes(content.code)) {
          this.info = { show: true,  message: 'There was a problem with your subscription', class: 'alert alert-danger', persist: true };
        } else if (content.code === '0007') {
          this.info = { show: true,  message: 'You are not authorized to see this content', class: 'alert alert-danger', persist: true };
        } else {
          this.info = { show: true,  message: 'Something went wrong...', class: 'alert alert-danger', persist: true };
        }
      });
    } else {
      this.info = { show: true,  message: 'Something went wrong...', class: 'alert alert-danger', persist: true };
    }
  }

}
