class ExcelInformeUno {
  rpTitleName: string;
  showName: string;
  duration:string;
  videoViews:Number;
  watchTimeMins:string;
  videoViews25:string;
  videoViews50:string;
  videoViews75:string;
  videoViews100:string;

} 
export const data:ExcelInformeUno[]=[
  {
    rpTitleName: 'tipoUno',
    showName: 'pruebas',
    duration:'2',
    videoViews:33,
    watchTimeMins:'13',
    videoViews25:'10',
    videoViews50:'7',
    videoViews75:'2',
    videoViews100:'3',
  },
  {
    rpTitleName: 'tipo2',
    showName: 'pruebas',
    duration:'2',
    videoViews:123,
    watchTimeMins:'14',
    videoViews25:'10',
    videoViews50:'2',
    videoViews75:'2',
    videoViews100:'3',
  },
  {
    rpTitleName: 'tipo3',
    showName: 'pruebas',
    duration:'2',
    videoViews:333,
    watchTimeMins:'14',
    videoViews25:'10',
    videoViews50:'7',
    videoViews75:'2',
    videoViews100:'1',
  },
  {
    rpTitleName: 'tipo4',
    showName: 'pruebas',
    duration:'2',
    videoViews:122,
    watchTimeMins:'15',
    videoViews25:'10',
    videoViews50:'7',
    videoViews75:'2',
    videoViews100:'3',
  },
]
 