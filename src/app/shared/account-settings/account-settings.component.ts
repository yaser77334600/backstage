import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../providers/settings.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: [
  ]
})
export class AccountSettingsComponent implements OnInit {

  // Propiedades de Clase
  public  linkTheme = document.querySelector('#theme');
  public linksS: NodeListOf<Element>;


  constructor( private settingsService:SettingsService ) { }

  ngOnInit(): void {
    this.settingsService.checkCurrentTime();
  }

  changeTheme( theme: string ){
    this.settingsService.changeTheme(theme);
  }


}
