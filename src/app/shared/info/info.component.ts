import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styles: []
})
export class InfoComponent  {
  @Input() info;
  constructor() { }

  countDown() {
    setTimeout(() => this.info.show = false, 5000);
  }
}
