import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent{

  constructor( public auth: AuthService){ }

  logout(){
    this.auth.logOut()
    localStorage.removeItem('theme');
  }

}
