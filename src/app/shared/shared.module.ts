import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BreadcrumpsComponent } from './breadcrumps/breadcrumps.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { InfoComponent } from './info/info.component';
import { PaginationComponent } from './pagination/pagination.component';
import { VsplayerComponent } from './vsplayer/vsplayer.component';
import { VsplayerV2Component } from './vsplayer-v2/vsplayer-v2.component';

import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';

import { SanitizePipe } from './pipes/sanitize.pipe';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { AccountSettingsComponent } from './account-settings/account-settings.component';


@NgModule({
  declarations: [
    InfoComponent,
    PaginationComponent,
    SanitizePipe,
    VsplayerComponent,
    VsplayerV2Component,
    NavbarComponent,

    BreadcrumpsComponent,
    SidebarComponent,
    HeaderComponent,
    AccountSettingsComponent,
  ],
  exports: [
    BreadcrumpsComponent,
    SidebarComponent,
    HeaderComponent,
    NavbarComponent,
    
    InfoComponent,
    PaginationComponent,
    SanitizePipe,
    VsplayerComponent,
    VsplayerV2Component,
    AccountSettingsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,        
  ]
})
export class SharedModule { }
