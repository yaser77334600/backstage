import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import videojs from 'video.js';

@Component({
  selector: 'app-vsplayer',
  template: `<video #target class="video-js" controls muted playsinline preload="none"></video>`,
  styles: [],
  encapsulation: ViewEncapsulation.None,
})
export class VsplayerComponent implements OnInit, OnDestroy {
  @ViewChild('target', {static: true}) target: ElementRef;
  @Input() options: {
      fluid: boolean,
      aspectRatio: string,
      autoplay: boolean,
      sources: { src: string, type: string, }[],
  };
  player: videojs.Player;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    // instantiate Video.js
    this.player = videojs(this.target.nativeElement, this.options, function onPlayerReady() {
      console.log('onPlayerReady', this);
    });
  }

  ngOnDestroy() {
    if (this.player) { this.player.dispose(); }
  }
}
