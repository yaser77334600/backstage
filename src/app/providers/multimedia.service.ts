import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { environment } from 'src/environments/environment.prod';
import { createFormData } from '../utils';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MultimediaService {
  constructor(private http: HttpClient, private auth: AuthService) { }

  public getMultimedia(query?: { }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    const data = createFormData(query);
    console.log(data)
    return this.http.get(environment.APIURI + 'multimedia?' + data, { headers });
  }

  public playMedia(id: string) {
    const headers = { 'Content-Type': 'application/json' };
    const data = createFormData({ tk: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'multimedia/play/' + id + '/?' + data, { headers });
  }

  public getSingleMultimedia(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'multimedia/' + id, { headers });
  }

  public getMultimediaByName(name: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'multimedia/byname/' + name, { headers });
  }

  public createMedia(multimedia) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'multimedia', multimedia, { headers });
  }

  public updateMedia(multimedia, id) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'multimedia/' + id, multimedia, { headers });
  }

  public addSource(multimediaID: string, media) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'multimedia/addsource/' + multimediaID, media, { headers });
  }

  public updateSource(mediaID: string, media) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'sources/' + mediaID, media, { headers });
  }

  public setMasterList(multimediaID: string, master) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'multimedia/setmaster/' + multimediaID, master, { headers });
  }

  public getSource(source: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'sources/' + source, { headers });
  }

  public uploadMedia(multimedia: string, file, query = {}) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    const data = createFormData(query);
    upload.append('file', file, file.name);
    return this.http.post(environment.APIURI + 'multimedia/upload/' + multimedia + '/media?' + data, upload,
    { headers, reportProgress: true, observe: 'events' })
    .pipe(map((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };
        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));
  }

  public uploadAudio(source: string, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'sources/audios/' + source, upload,
    { headers, reportProgress: true, observe: 'events' })
    .pipe(map((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };
        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));
  }

  public uploadSub(source: string, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'sources/subtitles/' + source, upload,
    { headers, reportProgress: true, observe: 'events' })
    .pipe(map((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };
        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));
  }

  public uploadCover(multimedia: string, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    upload.append('file', file, file.name);
    return this.http.post(environment.APIURI + 'sources/upload/' + multimedia + '/cover', upload,
    { headers, reportProgress: true, observe: 'events' })
    .pipe(map((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };
        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));
  }

  public updateMainCover(media: string, file, query: {}) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    const data = createFormData(query);
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'multimedia/upload/' + media + '/cover?' + data, upload, { headers });
  }

  public updateSourceCover(media: string, file, query: {}) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    const data = createFormData(query);
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'sources/upload/' + media + '/cover?' + data, upload, { headers });
  }
}
