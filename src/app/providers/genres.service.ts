import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { createFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class GenresService {
  constructor(private http: HttpClient, private auth: AuthService) { }

  public getGenres(query? ) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    const data = createFormData(query);
    return this.http.get(environment.APIURI + 'genres?' + data, { headers });
  }

  public getGenre(id) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'genres/' + id, { headers });
  }

  public createGenre(genre) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'genres', genre, { headers });
  }

  public deleteGenre(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'genres/' + id, { headers });
  }

  public updateGenre(id: string, update) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'genres/' + id, update, { headers });
  }

  public uploadMedia(id: string, file: FormData) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'sources/upload/' + id, file, { headers });
  }
}
