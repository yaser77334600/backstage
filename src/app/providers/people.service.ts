import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { createFormData } from '../utils';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  constructor(private http: HttpClient, private auth: AuthService) { }

  public getPerson(id) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'persons/' + id, { headers });
  }

  public getPersonByName(name) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'persons/byname/' + name, { headers });
  }

  public getPeople(query) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    const data = createFormData(query);
    return this.http.get(environment.APIURI + 'persons?' + data, { headers });
  }

  public createPerson(person) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'persons', person, { headers });
  }

  public deletePerson(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'persons/' + id, { headers });
  }

  public updatePerson(id: string, update) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'persons/' + id, update, { headers });
  }

  public updatePhoto(id: string, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'persons/' + id + '/upload', upload, { headers });
  }
}
