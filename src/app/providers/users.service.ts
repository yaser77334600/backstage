import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
// import { environment } from '../src/environments/environment.prod';
import { environment } from '../../environments/environment.prod';

import { createFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient, private auth: AuthService) { }

  public getUser(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'users/' + id, { headers });
  }

  public getUsers(query?: {}) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    const data = createFormData(query);
    return this.http.get(environment.APIURI + 'users?' + data, { headers });
  }

  public createUser(user) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'users', user, { headers });
  }

  public deleteUser(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'users/' + id, { headers });
  }

  public updateUser(id: string, update) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'users/' + id, update, { headers });
  }
}
