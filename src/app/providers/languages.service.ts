import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
// import LanguageModel from "../languages/models/languageModel";

@Injectable({
  providedIn: 'root'
})
export class LanguagesService {
  constructor(private http: HttpClient, private auth: AuthService) { }

  public getLanguages() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'languages', { headers });
  }

  public createLanguages(language: string) {
    const body = {
      'code': language
    };
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'languages', body, { headers });
  }

  public deleteLanguages(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'languages/' + id, { headers });
  }

  public updateLanguages(id: string, update) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'languages/' + id, update, { headers });
  }
}
