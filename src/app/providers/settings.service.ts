import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public  linkTheme = document.querySelector('#theme');

  constructor() { 
    const url = localStorage.getItem('theme') || './assets/css/colors/default-dark.css';
    this.linkTheme.setAttribute('href', url);
  }

  changeTheme( theme: string ){
    // console.log(theme);

    this.linkTheme = document.querySelector('#theme');
    const url = `./assets/css/colors/${theme}.css`;

    // Para cambiar la url que tenemos en el index usamos
    this.linkTheme.setAttribute('href', url);
    localStorage.setItem('theme', url);
    // console.log( linkTheme );
    // console.log( url );

    this.checkCurrentTime();
  }

  checkCurrentTime(){

    // Esto lo hacemos con js puro
    // queryselectorAll para llamar todos los que cumplem con una condicion
    // en el parametro llamamos el nombre de la clase que queremos llamar
    // console.log(this.linksS);
    const linksS = document.querySelectorAll('.selector');


    linksS.forEach( elem =>{
      elem.classList.remove('working')
      const btnTheme = elem.getAttribute('data-theme');

      // Ponemos cual es el url que deberia tener en el elemnto html que tengo en el enlace
      const themeUrl = `./assets/css/colors/${btnTheme}.css`

      // obtenemos el atributo de los linktheme
      const currenTheme = this.linkTheme.getAttribute('href');

      if( themeUrl === currenTheme ){
        elem.classList.add('working');
      }
    })

  }

}
