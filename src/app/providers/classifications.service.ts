import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ClassificationsService {
  constructor(private http: HttpClient, private auth: AuthService) { }

  public getClassifications() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'classifications', { headers });
  }

  public createClassification(classification) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.post(environment.APIURI + 'classifications', classification, { headers });
  }

  public deleteClassification(id: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'classifications/' + id, { headers });
  }

  public updateClassification(id: string, update) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.auth.returnToken() });
    return this.http.put(environment.APIURI + 'classifications/' + id, update, { headers });
  }
}
